#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in float color;
layout(location = 0) out vec4 out_color;

void main() {
	out_color = color * vec4(0.2, 0.2, 0.2, 1.);
}