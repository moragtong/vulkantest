#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(push_constant) uniform PushConstants {
	mat4 model, proj_view;
} push_constants;

layout(binding = 0) uniform Uniform {
	vec3 light;
} global;

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;

void main() {
	gl_Position = push_constants.model * vec4(position, 1.0);
}