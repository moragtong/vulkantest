#[macro_use]
extern crate glsl_to_spirv_macros;

#[macro_use]
extern crate glsl_to_spirv_macros_impl;

use std::iter::FromIterator;
use base::cgmath::One;

mod util;
mod base;

const ERROR_WINDOW_CREATION_FAILED: u32 = 24;
const ERROR_NO_OBJ_AVAILABLE: u32 = 40;
const ERROR_FILE_INVALID_OBJ: u32 = 42;

#[repr(C)]
#[derive(Clone, Copy)]
struct Uniform {
	light:		base::cgmath::Point3<f32>,
}

#[repr(C)]
#[derive(Clone, Copy)]
pub struct PushConstants {
	model:				base::cgmath::Matrix4<f32>,
	proj_view:			base::cgmath::Matrix4<f32>,
	//proj_view_model:	base::cgmath::Matrix4<f32>,
}

fn main() -> base::VkResult<()> {
	let filename = std::path::PathBuf::from(std::env::args().nth(1)
		.ok_or(ERROR_NO_OBJ_AVAILABLE)?);

	let obj = base::obj::Obj::<base::obj::SimplePolygon>::load(&filename)
		.ok().ok_or(ERROR_FILE_INVALID_OBJ)?;

	let indices = &obj.objects[0].groups[0].polys;

	let mut events_loop = base::winit::EventsLoop::new();

	let window = base::winit::Window::new(&events_loop)
		.ok().ok_or(ERROR_WINDOW_CREATION_FAILED)?;

	let instance = base::Instance::new(&base::get_required_extensions(&window))?;

	let surface = base::Surface::new(&instance, &window)?;

	let physicals = instance.get_physical_devices()?;

	let device = base::Device::new(&surface, &physicals)?;

	let vertex = base::ShaderModule::new(&device, include_glsl_vs!("src/shadow.vert"))?;
	let geom = base::ShaderModule::new(&device, include_glsl_gs!("src/shadow.geom"))?;
	let frag = base::ShaderModule::new(&device, include_glsl_fs!("src/main.frag"))?;

	let swapchain = base::SwapChain::new(&device, &surface)?;

	let descriptor_set_layout = base::DescriptorSetLayout::new(&device)?;

	let pipeline_layout = base::PipelineLayout::new(&descriptor_set_layout)?;

	let render_pass = base::RenderPass::new(&device)?;

	let graphics_pipeline = base::GraphicsPipeline::new(vertex, frag, Some(geom), &pipeline_layout, &render_pass)?;

	let depth_images = base::VkResult::<Vec<_>>::from_iter((0..swapchain.images().len())
		.map(|_| base::DepthImage::new(&device)))?.into_boxed_slice();

	let framebuffers = base::VkResult::<Vec<_>>::from_iter(swapchain.images().iter().zip(depth_images.iter()).map(|(image, depth_image)|
		base::Framebuffer::new(&render_pass, base::ImageView::new(&device, image)?, base::DepthImageView::new(depth_image)?)
	))?.into_boxed_slice();

	let command_pool = base::CommandPool::new(&device)?;

	let command_buffers = command_pool.allocate(swapchain.images().len())?;

	let shader_storage = Uniform {
		light:		base::cgmath::Point3::new(1.0, 1.0, 0.5)
	};

	let memory_buffer = base::MemoryBuffer::new(&device,
		std::mem::size_of::<Uniform>() + 
		obj.position.len() * std::mem::size_of::<base::PerVertex>() +
		indices.len() * std::mem::size_of::<[u32; 6]>(),
		base::vk_sys::BUFFER_USAGE_TRANSFER_DST_BIT |
		base::vk_sys::BUFFER_USAGE_VERTEX_BUFFER_BIT |
		base::vk_sys::BUFFER_USAGE_INDEX_BUFFER_BIT |
		base::vk_sys::BUFFER_USAGE_UNIFORM_BUFFER_BIT,
		base::vk_sys::MEMORY_PROPERTY_DEVICE_LOCAL_BIT)?;

	if let Some(command_buffer) = command_buffers.first() {
		let staging_buffer = base::MemoryBuffer::new(&device, memory_buffer.len() as _,
			base::vk_sys::BUFFER_USAGE_TRANSFER_SRC_BIT, base::vk_sys::MEMORY_PROPERTY_HOST_VISIBLE_BIT | base::vk_sys::MEMORY_PROPERTY_HOST_COHERENT_BIT)?;
	
		let staging_buffer_slice = unsafe { staging_buffer.get_unchecked(0..staging_buffer.len()) };
		{
			let mut mapping = staging_buffer_slice.map_memory()?;
			*mapping.as_mut() = shader_storage;
			unsafe {
				base::PerVertex::fill(&obj.position, indices, std::slice::from_raw_parts_mut(
					mapping.get_unchecked_mut(
						std::mem::size_of::<Uniform>()..
						std::mem::size_of::<Uniform>() + obj.position.len() * std::mem::size_of::<base::PerVertex>())
				.as_mut_ptr() as _, obj.position.len()));

				base::build_adjacancy_list(indices, std::slice::from_raw_parts_mut(
					mapping.get_unchecked_mut(
						std::mem::size_of::<Uniform>() + obj.position.len() * std::mem::size_of::<base::PerVertex>()..)
					.as_mut_ptr() as _, indices.len()
				));
			}
		}

		let recording = command_buffer.acquire()?.record()?;
		recording.copy(&unsafe { memory_buffer.get_unchecked(0..memory_buffer.len() as _) }, &staging_buffer_slice);

		recording.end()?;
		command_buffer.submit()?;
		command_buffer.acquire()?;
	}

	let uniform_slice = unsafe { memory_buffer.get_unchecked(0..std::mem::size_of::<Uniform>() as _) };

	let descriptor_pool = base::DescriptorPool::new(&device, 1)?;

	let descriptor_set = [descriptor_pool.allocate_one(&descriptor_set_layout)?];

	device.update_descriptor_set(&descriptor_set[0], &uniform_slice);

	let mut push_constants = PushConstants {
		model:				One::one(),
		//proj_view_model:	One::one(),
		proj_view:	One::one(),
	};

	let proj = base::cgmath::perspective(base::cgmath::Deg(90.), device.extent.width as f32 / device.extent.height as f32, 0.1, 5.);
	let mut view = base::cgmath::Matrix4::look_at(base::cgmath::Point3::new(2., 2., 2.), base::cgmath::Point3::new(0., 0., 0.), base::cgmath::Vector3::new(1., 0., 0.));

	println!("Finished creating");

	let vertex_buffer_slice = unsafe { memory_buffer.get_unchecked(
		std::mem::size_of::<Uniform>() as _..
		(std::mem::size_of::<Uniform>() + obj.position.len() * std::mem::size_of::<base::PerVertex>()) as _) };

	let index_buffer_slice = unsafe { memory_buffer.get_unchecked(
		(std::mem::size_of::<Uniform>() + obj.position.len() * std::mem::size_of::<base::PerVertex>()) as _..
		memory_buffer.len()) };

	let mut iter = command_buffers.iter().zip(framebuffers.iter()).cycle();

	let mut should_close = false;

	loop {
		events_loop.poll_events(|event| {
			match event {
				base::winit::Event::WindowEvent { event: base::winit::WindowEvent::Closed, .. }
					=> should_close = true,
				base::winit::Event::WindowEvent {
					event: base::winit::WindowEvent::KeyboardInput {
						input:	base::winit::KeyboardInput {
							state:				base::winit::ElementState::Pressed,
							virtual_keycode:	Some(keycode),
							..
						}, ..
					}, ..
				} =>
					match keycode {
						base::winit::VirtualKeyCode::A =>
							view.w.x += 0.1,
						base::winit::VirtualKeyCode::D =>
							view.w.x -= 0.1,
						base::winit::VirtualKeyCode::W =>
							view.w.y += 0.1,
						base::winit::VirtualKeyCode::S =>
							view.w.y -= 0.1,
						base::winit::VirtualKeyCode::E =>
							view.w.z += 0.1,
						base::winit::VirtualKeyCode::Q =>
							view.w.z -= 0.1,
						base::winit::VirtualKeyCode::H =>
							push_constants.model.w.x += 0.1,
						base::winit::VirtualKeyCode::K =>
							push_constants.model.w.x -= 0.1,
						base::winit::VirtualKeyCode::U =>
							push_constants.model.w.y += 0.1,
						base::winit::VirtualKeyCode::J =>
							push_constants.model.w.y -= 0.1,
						base::winit::VirtualKeyCode::I =>
							push_constants.model.w.z += 0.1,
						base::winit::VirtualKeyCode::Y =>
							push_constants.model.w.z -= 0.1,
						_ => {}
					}
				_ => {}
			}
		});

		if should_close {
			break
		}

		push_constants.proj_view = proj * view;

		//push_constants.proj_view_model = proj * view * push_constants.model;

		if let Some((command_buffer, framebuffer)) = iter.next() {
			let swapchain_image = swapchain.acquire_next_image(command_buffer)?;
			let recording = command_buffer.acquire()?.record()?;
			{
				let record_render_pass = recording.render_pass(&render_pass, framebuffer);
				record_render_pass.bind_pipeline(&graphics_pipeline);
				record_render_pass.bind_descriptor_sets(&pipeline_layout, &descriptor_set);
				record_render_pass.push_constants(&pipeline_layout, &push_constants);
				record_render_pass.bind_vertex_buffers(&vertex_buffer_slice);
				record_render_pass.bind_index_buffer(&index_buffer_slice);
				record_render_pass.draw_indexed(indices.len() as u32 * 6);
			}
			recording.end()?;
			swapchain_image.submit()?;
			swapchain_image.present()?;
		}
	}

	base::Fence::wait(&command_buffers)?;

	Ok(())
}