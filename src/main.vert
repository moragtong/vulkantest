#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(push_constant) uniform PushConstants {
	mat4 model, proj_view_model;
} push_constants;

layout(binding = 0) uniform Uniform {
	vec3 light;
} global;

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;

layout(location = 0) out float color;

void main() {
	gl_Position = push_constants.proj_view_model * vec4(position, 1.0);
	float dist = distance((push_constants.model * vec4(position, 1.0)).xyz, global.light);
	color = clamp(dot(normal, normalize(position - global.light)), 0, 1) / dist / dist;
}