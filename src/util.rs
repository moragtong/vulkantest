/// An attempted conversion that consumes `self`, which may or may not be
/// expensive.
///
/// Library authors should not directly implement this trait, but should prefer
/// implementing the [`TryFrom`] trait, which offers greater flexibility and
/// provides an equivalent `TryInto` implementation for free, thanks to a
/// blanket implementation in the standard library. For more information on this,
/// see the documentation for [`Into`].
///
/// [`TryFrom`]: trait.TryFrom.html
/// [`Into`]: trait.Into.html
pub trait TryInto<T>: Sized {
	/// The type returned in the event of a conversion error.
	type Error;

	/// Performs the conversion.
	fn try_into(self) -> Result<T, Self::Error>;
}

pub trait TryFrom<T>: Sized {
	/// The type returned in the event of a conversion error.
	type Error;

	/// Performs the conversion.
	fn try_from(value: T) -> Result<Self, Self::Error>;
}

impl<T, U> TryInto<U> for T where U: TryFrom<T> {
	type Error = U::Error;

	fn try_into(self) -> Result<U, U::Error> {
		U::try_from(self)
	}
}

macro_rules! array_impls {
	($($N:expr)+) => {
		$(
			impl<'a, T> TryFrom<&'a [T]> for &'a [T; $N] {
				type Error = &'static str;

				fn try_from(slice: &[T]) -> Result<&[T; $N], Self::Error> {
					if slice.len() == $N {
						let ptr = slice.as_ptr() as *const [T; $N];
						unsafe { Ok(&*ptr) }
					} else {
						Err("wrong number of parameters")
					}
				}
			}

			impl<'a, T> TryFrom<&'a mut [T]> for &'a mut [T; $N] {
				type Error = &'static str;

				fn try_from(slice: &mut [T]) -> Result<&mut [T; $N], Self::Error> {
					if slice.len() == $N {
						let ptr = slice.as_mut_ptr() as *mut [T; $N];
						unsafe { Ok(&mut *ptr) }
					} else {
						Err("wrong number of parameters")
					}
				}
			}
		)+
	}
}

array_impls!{ 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 }