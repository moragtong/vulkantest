use std;
use super::*;

pub struct DescriptorPool<'inst, 'dev>
where 'inst: 'dev {
	pub(super) device:	&'dev Device<'inst>,
	pub(super) pool:	vk_sys::DescriptorPool,
}

pub struct DescriptorSet<'inst, 'dev, 'pool>(pub(super) vk_sys::DescriptorSet, std::marker::PhantomData<&'pool &'dev &'inst ()>)
where 'inst: 'dev, 'dev: 'pool;

impl<'inst, 'dev> DescriptorPool<'inst, 'dev> {
	pub fn new(device: &'dev Device<'inst>, count: u32) -> VkResult<Self> {
		unsafe {
			let mut pool = std::mem::uninitialized();
			let res = device.procs.CreateDescriptorPool(device.device, &vk_sys::DescriptorPoolCreateInfo {
				sType:			vk_sys::STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
				maxSets:		1,
				poolSizeCount:	1,
				pPoolSizes:		&vk_sys::DescriptorPoolSize {
					ty:					vk_sys::DESCRIPTOR_TYPE_UNIFORM_BUFFER,
					descriptorCount:	count,
				}, ..std::mem::zeroed()
			}, 0 as _, &mut pool);

			if res == vk_sys::SUCCESS {
				Ok(Self { device, pool })
			} else {
				Err(res)
			}
		}
	}

	pub fn allocate_one(&self, layout: &DescriptorSetLayout<'inst, 'dev>) -> VkResult<DescriptorSet> {
		unsafe {
			let mut ret = std::mem::uninitialized();

			let res = self.device.procs.AllocateDescriptorSets(self.device.device, &vk_sys::DescriptorSetAllocateInfo {
				sType:				vk_sys::STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
				descriptorPool:		self.pool,
				descriptorSetCount:	1,
				pSetLayouts:		&layout.layout,
				..std::mem::zeroed()
			}, &mut ret as *mut _ as _);

			if res == vk_sys::SUCCESS {
				Ok(ret)
			} else {
				Err(res)
			}
		}
	}

	/*pub fn allocate(&self, layouts: &[DescriptorSetLayout<'inst, 'dev>]) -> VkResult<Vec<DescriptorSet>> {
		let layouts = Vec::from_iter(layouts.iter().map(|layout| layout.layout));
		unsafe {
			let mut ret = Vec::with_capacity(layouts.len());
			let res = self.device.procs.AllocateDescriptorSets(self.device.device, &vk_sys::DescriptorSetAllocateInfo {
				sType:				vk_sys::STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
				descriptorPool:		self.pool,
				descriptorSetCount:	layouts.len() as _,
				pSetLayouts:		layouts.as_ptr(),
				..std::mem::zeroed()
			}, ret.as_mut_ptr() as _);

			if res == vk_sys::SUCCESS {
				ret.set_len(layouts.len());
				Ok(ret)
			} else {
				Err(res)
			}
		}
	}*/
}

impl<'inst, 'dev> Drop for DescriptorPool<'inst, 'dev> {
	fn drop(&mut self) {
		unsafe {
			self.device.procs.DestroyDescriptorPool(self.device.device, self.pool, 0 as _);
		}
	}
}