use std;
use super::*;

pub struct GraphicsPipeline<'inst, 'dev, 'desc_layout, 'layout, T>
where 'inst: 'dev, 'dev: 'desc_layout, 'desc_layout: 'layout, T: Clone + Copy {
	_vertex:				ShaderModule<'inst, 'dev>,
	_geom:					Option<ShaderModule<'inst, 'dev>>,
	_frag:					ShaderModule<'inst, 'dev>,
	pub(super) pipeline:	vk_sys::Pipeline,
	marker1:				std::marker::PhantomData<&'layout &'desc_layout ()>,
	marker2:				std::marker::PhantomData<T>,
}

impl<'inst, 'surf, 'dev, 'desc_layout, 'layout, T: Clone + Copy> GraphicsPipeline<'inst, 'dev, 'desc_layout, 'layout, T> {
	pub fn new(
		_vertex:		ShaderModule<'inst, 'dev>,
		_frag:			ShaderModule<'inst, 'dev>,
		_geom:			Option<ShaderModule<'inst, 'dev>>,
		layout:			&'layout PipelineLayout<'inst, 'dev, 'desc_layout, T>,
		render_pass:	&RenderPass<'inst, 'dev>,
	) -> VkResult<Self> {
		unsafe {
			let create_infos = [
				vk_sys::PipelineShaderStageCreateInfo {
					sType:	vk_sys::STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
					stage:	vk_sys::SHADER_STAGE_VERTEX_BIT,
					module:	_vertex.module,
					pName:	b"main\0".as_ptr() as _,
					..std::mem::zeroed()
				},
				vk_sys::PipelineShaderStageCreateInfo {
					sType:	vk_sys::STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
					stage:	vk_sys::SHADER_STAGE_FRAGMENT_BIT,
					module:	_frag.module,
					pName:	b"main\0".as_ptr() as _,
					..std::mem::zeroed()
				},
				vk_sys::PipelineShaderStageCreateInfo {
					sType:	vk_sys::STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
					stage:	vk_sys::SHADER_STAGE_GEOMETRY_BIT,
					module:	_geom.as_ref().map_or(0, |shader| shader.module),
					pName:	b"main\0".as_ptr() as _,
					..std::mem::zeroed()
				},
			];

			let mut pipeline: vk_sys::Pipeline = std::mem::uninitialized();
			let res = render_pass.device.procs.CreateGraphicsPipelines(render_pass.device.device, 0, 1, &vk_sys::GraphicsPipelineCreateInfo {
				sType:					vk_sys::STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
				stageCount:				create_infos.len() as u32 - if _geom.is_some() { 0 } else { 1 },
				pStages:				create_infos.as_ptr(),
				layout:					layout.layout,
				renderPass:				render_pass.render_pass,
				pVertexInputState:		&vk_sys::PipelineVertexInputStateCreateInfo {
					sType:								vk_sys::STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
					vertexAttributeDescriptionCount:	PerVertex::ATTRIBUTE_DESCRIPTIONS.len() as _,
					pVertexAttributeDescriptions:		PerVertex::ATTRIBUTE_DESCRIPTIONS.as_ptr(),
					vertexBindingDescriptionCount:		1,
					pVertexBindingDescriptions:			&vk_sys::VertexInputBindingDescription {
						binding:	0,
						stride:		std::mem::size_of::<PerVertex>() as _,
						inputRate:	vk_sys::VERTEX_INPUT_RATE_VERTEX,
					}, ..std::mem::zeroed()
				},
				pDepthStencilState:		&vk_sys::PipelineDepthStencilStateCreateInfo {
					sType:				vk_sys::STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
					depthTestEnable:	vk_sys::TRUE,
					depthWriteEnable:	vk_sys::TRUE,
					depthCompareOp:		vk_sys::COMPARE_OP_LESS,
					..std::mem::zeroed()
				},
				pInputAssemblyState:	&vk_sys::PipelineInputAssemblyStateCreateInfo {
					sType:		vk_sys::STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
					topology:	vk_sys::PRIMITIVE_TOPOLOGY_TRIANGLE_LIST_WITH_ADJACENCY,
					..std::mem::zeroed()
				},
				pViewportState:			&vk_sys::PipelineViewportStateCreateInfo {
					sType:			vk_sys::STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
					viewportCount:	1,
					pViewports:		&vk_sys::Viewport {
						width:		render_pass.device.extent.width as _,
						height:		render_pass.device.extent.height as _,
						maxDepth:	1.,
						..std::mem::zeroed()
					},
					scissorCount:	1,
					pScissors:		&vk_sys::Rect2D { extent: std::mem::transmute_copy(&render_pass.device.extent), ..std::mem::zeroed() },
					..std::mem::zeroed()
				},
				pRasterizationState:	&vk_sys::PipelineRasterizationStateCreateInfo {
					sType:			vk_sys::STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
					polygonMode:	vk_sys::POLYGON_MODE_FILL,
					lineWidth:		1.,
					cullMode:		vk_sys::CULL_MODE_BACK_BIT,
					frontFace:		vk_sys::FRONT_FACE_COUNTER_CLOCKWISE,
					..std::mem::zeroed()
				},
				pMultisampleState:		&vk_sys::PipelineMultisampleStateCreateInfo {
					sType:					vk_sys::STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
					rasterizationSamples:	vk_sys::SAMPLE_COUNT_1_BIT,
					..std::mem::zeroed()
				},
				pColorBlendState:		&vk_sys::PipelineColorBlendStateCreateInfo {
					sType:				vk_sys::STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
					attachmentCount:	1,
					pAttachments:		&vk_sys::PipelineColorBlendAttachmentState {
						colorWriteMask:	vk_sys::COLOR_COMPONENT_R_BIT |
										vk_sys::COLOR_COMPONENT_G_BIT |
										vk_sys::COLOR_COMPONENT_B_BIT |
										vk_sys::COLOR_COMPONENT_A_BIT,
						..std::mem::zeroed()
					},
					..std::mem::zeroed()
				},
				..std::mem::zeroed()
			}, 0 as _, &mut pipeline);
			if res == vk_sys::SUCCESS {
				Ok(GraphicsPipeline { _vertex, _geom, _frag, pipeline, marker1: Default::default(), marker2: Default::default() })
			} else {
				Err(res)
			}
		}
	}
}

impl<'inst, 'dev, 'desc_layout, 'layout, T: Clone + Copy> Drop for GraphicsPipeline<'inst, 'dev, 'desc_layout, 'layout, T>{
	fn drop(&mut self) {
		unsafe { self._vertex.device.procs.DestroyPipeline(self._vertex.device.device, self.pipeline, 0 as _) };
		println!("GraphicsPipeline destructed");
	}
}