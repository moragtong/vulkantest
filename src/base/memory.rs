use std;
use super::*;

const ERROR_SUITABLE_MEMORY_TYPES_NOT_FOUND: u32 = 26;

pub(super) struct Memory<'inst, 'dev>
where 'inst: 'dev {
	pub(super) device:	&'dev Device<'inst>,
	pub(super) memory:	vk_sys::DeviceMemory,
	pub(super) len:		vk_sys::DeviceSize,
}

impl<'inst, 'dev> Memory<'inst, 'dev> {
	pub(super) fn new(device: &'dev Device<'inst>, requirements: &vk_sys::MemoryRequirements, properties: u32) -> VkResult<Self> {
		unsafe {
			let mut memory = std::mem::uninitialized();

			let res = device.procs.AllocateMemory(device.device, &vk_sys::MemoryAllocateInfo {
				sType:				vk_sys::STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
				allocationSize:		requirements.size,
				memoryTypeIndex:	{
					let device_memory_properties = device.surface.get_physical_device_memory_properties(device.physical);

					device_memory_properties.memoryTypes.get_unchecked(..device_memory_properties.memoryTypeCount as _).iter().enumerate()
						.find(|&(i, memory_type)| (requirements.memoryTypeBits & (1 << i as u32) != 0) && (memory_type.propertyFlags & properties != 0))
						.ok_or(ERROR_SUITABLE_MEMORY_TYPES_NOT_FOUND)?.0 as _
				},
				..std::mem::zeroed()
			}, 0 as _, &mut memory);

			if res == vk_sys::SUCCESS {
				Ok(Self { device, memory, len: requirements.size })
			} else {
				Err(res)
			}
		}
	}
}

impl<'inst, 'dev> Drop for Memory<'inst, 'dev> {
	fn drop(&mut self) {
		unsafe {
			self.device.procs.FreeMemory(self.device.device, self.memory, 0 as _);
		}

		println!("Memory destructed");
	}
}