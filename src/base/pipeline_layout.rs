use std;
use super::*;

pub struct PipelineLayout<'inst, 'dev, 'desc_layout, T>
where 'inst: 'dev, 'dev: 'desc_layout, T: Copy + Clone {
	pub(super) device:	&'dev Device<'inst>,
	pub(super) layout:	vk_sys::PipelineLayout,
	marker:				std::marker::PhantomData<T>,
	marker2:			std::marker::PhantomData<&'desc_layout ()>,
}

impl<'inst, 'dev, 'desc_layout, T: Copy + Clone> PipelineLayout<'inst, 'dev, 'desc_layout, T> {
	pub fn new(set_layout: &'desc_layout DescriptorSetLayout<'inst, 'dev>) -> VkResult<Self> {
		debug_assert!(std::mem::size_of::<T>() % 4 == 0);
		unsafe {
			let mut layout = std::mem::uninitialized();
			let res = set_layout.device.procs.CreatePipelineLayout(set_layout.device.device, &vk_sys::PipelineLayoutCreateInfo {
				sType:					vk_sys::STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
				setLayoutCount:			1,
				pSetLayouts:			&set_layout.layout,
				pushConstantRangeCount:	1,
				pPushConstantRanges:	&vk_sys::PushConstantRange {
					stageFlags:	vk_sys::SHADER_STAGE_VERTEX_BIT | vk_sys::SHADER_STAGE_GEOMETRY_BIT,
					offset:		0,
					size:		std::mem::size_of::<T>() as _
				},
				..std::mem::zeroed()
			}, 0 as _, &mut layout);
			if res == vk_sys::SUCCESS {
				Ok(Self { layout, device: set_layout.device, marker: Default::default(), marker2: Default::default() })
			} else {
				Err(res)
			}
		}
	}
}

impl<'inst, 'dev, 'desc_layout, T: Clone + Copy> Drop for PipelineLayout<'inst, 'dev, 'desc_layout, T> {
	fn drop(&mut self) {
		unsafe { self.device.procs.DestroyPipelineLayout(self.device.device, self.layout, 0 as _) };
		println!("PipelineLayout destructed");
	}
}