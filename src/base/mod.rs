use std;

pub extern crate vk_sys;
extern crate libloading;
extern crate libc;

pub extern crate cgmath;
pub extern crate winit;
pub extern crate obj;

/*#[macro_use]
extern crate static_assertions;*/

pub type VkResult<T> = Result<T, vk_sys::Result>;

mod memory_buffer;
pub use self::memory_buffer::*;
mod surface;
pub use self::surface::*;
mod memory_mapping;
pub use self::memory_mapping::*;
mod device;
pub use self::device::*;
mod pipeline_layout;
pub use self::pipeline_layout::*;
mod image_view;
pub use self::image_view::*;
mod semaphore;
pub use self::semaphore::*;
mod fence;
pub use self::fence::*;
mod shader_module;
pub use self::shader_module::*;
mod command_pool;
pub use self::command_pool::*;
mod swapchain_image;
pub use self::swapchain_image::*;
mod framebuffer;
pub use self::framebuffer::*;
mod renderpass;
pub use self::renderpass::*;
mod instance;
pub use self::instance::*;
mod command_buffer;
pub use self::command_buffer::*;
mod graphics_pipeline;
pub use self::graphics_pipeline::*;
mod swapchain;
pub use self::swapchain::*;
mod memory;
use self::memory::*;
mod depth_image;
pub use self::depth_image::*;
mod depth_image_view;
pub use self::depth_image_view::*;
mod descriptor_set_layout;
pub use self::descriptor_set_layout::*;
mod descriptor_pool;
pub use self::descriptor_pool::*;
mod per_vertex;
pub use self::per_vertex::*;

use self::winit::os::unix::WindowExt;

pub fn build_adjacancy_list(indices: &[obj::SimplePolygon], output: &mut [[u32; 6]]) {
	debug_assert!(indices.len() == output.len());

	let mut edges = std::collections::HashMap::new();

	for tri in indices.iter() {
		edges.insert((tri[0].0, tri[1].0), tri[2].0);
		edges.insert((tri[2].0, tri[0].0), tri[1].0);
		edges.insert((tri[1].0, tri[2].0), tri[0].0);
	}

	for (tri, adj) in indices.iter().zip(output.iter_mut()) {
		*adj = [
			tri[0].0 as _,
			edges[&(tri[1].0, tri[0].0)] as _,
			tri[1].0 as _,
			edges[&(tri[2].0, tri[1].0)] as _,
			tri[2].0 as _,
			edges[&(tri[0].0, tri[2].0)] as _,
		];
	}
}

#[cfg(unix)]
pub fn get_required_extensions(window: &winit::Window) -> [*const i8; 2] {
	[b"VK_KHR_surface\0".as_ptr() as _,
		if window.get_xlib_display().is_some() {
			b"VK_KHR_xlib_surface\0".as_ptr()
		} else if window.get_wayland_display().is_some() {
			b"VK_KHR_wayland_surface\0".as_ptr()
		} else {
			0 as _
		} as _
	]
}

#[cfg(window)]
pub fn get_required_extensions(window: &winit::Window) -> [*const i8; 2] {
	[b"VK_KHR_surface\0".as_ptr() as _, b"VK_KHR_win32_surface\0".as_ptr() as _]
}