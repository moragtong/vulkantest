use std;
use super::*;

#[cfg(unix)]
const VULKAN: &str = "libvulkan.so.1";

#[cfg(windows)]
const VULKAN: &str = "vulkan-1.dll";

#[cfg(debug_assertions)]
const VALIDATION_LAYER: &[u8] = b"VK_LAYER_LUNARG_standard_validation\0";

const ERROR_VULKAN_LIBRARY_LOADING_FAILED: u32 = 20;
const ERROR_FUNCTION_NOT_FOUND: u32 = 21;

#[cfg(debug_assertions)]
const ERROR_VALIDATION_LAYER_NOT_AVAILABLE: u32 = 25;

#[cfg(debug_assertions)]
fn enumerate_instance_layer_properties(procs: &vk_sys::EntryPoints) -> VkResult<Vec<vk_sys::LayerProperties>> {
	unsafe {
		let mut count = std::mem::uninitialized();
		let res = procs.EnumerateInstanceLayerProperties(&mut count, 0 as _);
		if res != vk_sys::SUCCESS {
			return Err(res)
		}
		let mut ret = Vec::with_capacity(count as _);
		if count != 0 {
			let res = procs.EnumerateInstanceLayerProperties(&mut count, ret.as_mut_ptr());
			if res != vk_sys::SUCCESS {
				return Err(res)
			}
			ret.set_len(count as _);
		}
		Ok(ret)
	}
}

pub struct Instance {
	_lib:					libloading::Library,
	pub(super) instance:	vk_sys::Instance,
	pub(super) procs:		vk_sys::InstancePointers,
}

impl Instance {
	pub(super) fn get_physical_device_queue_family_properties(&self, physical: vk_sys::PhysicalDevice, ret: &mut Vec<vk_sys::QueueFamilyProperties>) -> usize {
		unsafe {
			let mut count = std::mem::uninitialized();
			self.procs.GetPhysicalDeviceQueueFamilyProperties(physical, &mut count, 0 as _);
			ret.reserve(count as _);
			self.procs.GetPhysicalDeviceQueueFamilyProperties(physical, &mut count, ret.as_mut_ptr());
			let prevlen = ret.len();
			ret.set_len(prevlen + count as usize);
			count as _
		}
	}

	pub(super) fn get_device_extension_properties(&self, physical: vk_sys::PhysicalDevice, properties: &mut Vec<vk_sys::ExtensionProperties>) -> VkResult<usize> {
		unsafe {
			let mut count = std::mem::uninitialized();
			let res = self.procs.EnumerateDeviceExtensionProperties(physical, 0 as _, &mut count, 0 as _);
			if res != vk_sys::SUCCESS {
				return Err(res)
			}
			if count != 0 {
				properties.reserve(count as _);
				let res = self.procs.EnumerateDeviceExtensionProperties(physical, 0 as _, &mut count, properties.as_mut_ptr());
				if res != vk_sys::SUCCESS {
					return Err(res)
				}
				let prevlen = properties.len();
				properties.set_len(prevlen + count as usize);
			}
			Ok(count as _)
		}
	}

	pub fn get_physical_devices(&self) -> VkResult<Vec<vk_sys::PhysicalDevice>> {
		unsafe {
			let mut count = std::mem::uninitialized();

			let res = self.procs.EnumeratePhysicalDevices(self.instance, &mut count, 0 as _);

			if res != vk_sys::SUCCESS {
				return Err(res)
			}

			let mut physicals = Vec::with_capacity(count as _);

			if count != 0 {
				let res = self.procs.EnumeratePhysicalDevices(self.instance, &mut count, physicals.as_mut_ptr());
				if res == vk_sys::SUCCESS && count != 0 {
					physicals.set_len(count as _);
					Ok(physicals)
				} else {
					Err(res)
				}
			} else {
				Ok(physicals)
			}
		}
	}

	pub fn new(extensions: &[*const i8]) -> VkResult<Self> {
		let _lib = libloading::Library::new(VULKAN)
			.ok().ok_or(ERROR_VULKAN_LIBRARY_LOADING_FAILED)?;
		let get_instance_proc_addr: extern "system" fn(_: vk_sys::Instance, _: *const super::libc::c_char) -> *const std::os::raw::c_void = *unsafe { _lib.get(b"vkGetInstanceProcAddr\0") }
			.ok().ok_or(ERROR_FUNCTION_NOT_FOUND)?;

		let entry_points = vk_sys::EntryPoints::load(move |function| get_instance_proc_addr(0, function.as_ptr()));

		unsafe {
			#[cfg(debug_assertions)] {
				println!("Debug enabled");
				enumerate_instance_layer_properties(&entry_points)?.into_iter()
					.find(|property| libc::strncmp(VALIDATION_LAYER.as_ptr() as _, property.layerName.as_ptr(), property.layerName.len() as _) == 0)
					.ok_or(ERROR_VALIDATION_LAYER_NOT_AVAILABLE)?;
			}

			#[cfg(debug_assertions)]
			const LAYER_COUNT: u32 = 1;

			#[cfg(not(debug_assertions))]
			const LAYER_COUNT: u32 = 0;

			#[cfg(debug_assertions)]
			let enabled_validation_names = &(VALIDATION_LAYER.as_ptr() as _);

			#[cfg(not(debug_assertions))]
			let enabled_validation_names = 0 as _;

			let mut instance = std::mem::uninitialized();
			
			let res = entry_points.CreateInstance(&vk_sys::InstanceCreateInfo {
				sType:						vk_sys::STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
				ppEnabledExtensionNames:	extensions.as_ptr(),
				enabledExtensionCount:		extensions.len() as _,
				enabledLayerCount:			LAYER_COUNT,
				ppEnabledLayerNames:		enabled_validation_names,
				..std::mem::zeroed()
			}, 0 as _, &mut instance);

			if res == vk_sys::SUCCESS {
				Ok(Self { _lib, instance, procs: vk_sys::InstancePointers::load(move |function| get_instance_proc_addr(instance, function.as_ptr())) })
			} else {
				Err(res)
			}
		}
	}
}

impl Drop for Instance {
	fn drop(&mut self) {
		unsafe { self.procs.DestroyInstance(self.instance, 0 as _) };
		println!("Instance destructed");
	}
}