use std;
use super::*;

use std::iter::FromIterator;

pub struct Fence<'inst, 'dev, T>
where 'inst: 'dev {
	pub(super) device:	&'dev Device<'inst>,
	is_signaled:		std::cell::Cell<bool>,
	fence:				vk_sys::Fence,
	pub(super) object:	T,
}

impl<'inst, 'dev, T> Fence<'inst, 'dev, T> {
	pub fn new(device: &'dev Device<'inst>, object: T) -> VkResult<Self> {
		unsafe {
			let mut fence = std::mem::uninitialized();
			let res = device.procs.CreateFence(device.device, &vk_sys::FenceCreateInfo {
				sType:	vk_sys::STRUCTURE_TYPE_FENCE_CREATE_INFO,
				..std::mem::zeroed()
			}, 0 as _, &mut fence);
			if res == vk_sys::SUCCESS {
				Ok(Self { device, fence, object, is_signaled: Default::default() })
			} else {
				Err(res)
			}
		}
	}

	pub fn acquire(&self) -> VkResult<&T> {
		if self.is_signaled.take() {
			let res = unsafe { self.device.procs.WaitForFences(self.device.device, 1, &self.fence, vk_sys::TRUE, std::u64::MAX) };
			if res == vk_sys::SUCCESS {
				let res = unsafe { self.device.procs.ResetFences(self.device.device, 1, &self.fence) };
				if res == vk_sys::SUCCESS {
					Ok(&self.object)
				} else {
					Err(res)
				}
			} else {
				Err(res)
			}
		} else {
			Ok(&self.object)
		}
	}

	pub(super) fn signal(&self) -> vk_sys::Fence {
		self.is_signaled.set(true);
		self.fence
	}

	pub fn wait(selves: &[Self]) -> VkResult<()> {
		let arr = Vec::from_iter(selves.iter().map(|item| item.fence));
		if let Some(first) = selves.first() {
			let res = unsafe { first.device.procs.WaitForFences(first.device.device, arr.len() as _, arr.as_ptr(), vk_sys::TRUE, std::u64::MAX) };
			if res == vk_sys::SUCCESS {
				for fence in selves.iter() {
					fence.is_signaled.take();
				}
				Ok(())
			} else {
				Err(res)
			}
		} else {
			Ok(())
		}
	}
}

impl<'inst, 'dev, T> Drop for Fence<'inst, 'dev, T> {
	fn drop(&mut self) {
		unsafe { self.device.procs.DestroyFence(self.device.device, self.fence, 0 as _) };
		println!("Fence destructed");
	}
}