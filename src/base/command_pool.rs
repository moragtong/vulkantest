use std;
use super::*;

pub struct CommandPool<'inst, 'dev> 
where 'inst: 'dev {
	pub(super) device:	&'dev Device<'inst>,
	command_pool:		vk_sys::CommandPool,
}

impl<'inst, 'dev> CommandPool<'inst, 'dev> {
	pub fn new(device: &'dev Device<'inst>) -> VkResult<Self> {
		unsafe {
			let mut command_pool = std::mem::uninitialized();
			let res = device.procs.CreateCommandPool(device.device, &vk_sys::CommandPoolCreateInfo {
				sType:				vk_sys::STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
				flags:				vk_sys::COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
				queueFamilyIndex:	device.graphics_family_index,
				..std::mem::zeroed()
			}, 0 as _, &mut command_pool);

			if res == vk_sys::SUCCESS {
				Ok(Self { device, command_pool })
			} else {
				Err(res)
			}
		}
	}

	pub fn allocate<'pool>(&'pool self, count: usize) -> VkResult<Vec<Fence<CommandBuffer>>> {
		unsafe {
			let mut command_buffers = Vec::with_capacity(count);

			self.device.procs.AllocateCommandBuffers(self.device.device, &vk_sys::CommandBufferAllocateInfo {
				sType:				vk_sys::STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
				commandPool:		self.command_pool,
				level:				vk_sys::COMMAND_BUFFER_LEVEL_PRIMARY,
				commandBufferCount:	count as _,
				..std::mem::zeroed()
			}, command_buffers.as_mut_ptr() as _);
			command_buffers.set_len(count as _);

			command_buffers.into_iter().map(|command_buffer|
				Fence::new(self.device, CommandBuffer {
					command_buffer,
					semaphore:		super::semaphore::Semaphore::new(self.device)?,
					marker:			std::marker::PhantomData::<&'pool ()>
				})
			).collect()
		}
	}
}

impl<'inst, 'dev> Drop for CommandPool<'inst, 'dev> {
	fn drop(&mut self) {
		unsafe { self.device.procs.DestroyCommandPool(self.device.device, self.command_pool, 0 as _) };
		println!("CommandPool destructed");
	}
}