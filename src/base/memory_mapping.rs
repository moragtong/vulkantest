use std;
use super::*;

pub struct MemoryMapping<'inst, 'dev, 'membuf, 'membufslice>
where 'inst: 'dev, 'dev: 'membuf, 'membuf: 'membufslice {
	pub(super) memory_buffer_slice:	&'membufslice super::memory_buffer::MemoryBufferSlice<'inst, 'dev, 'membuf>,
	pub(super) buff:				*mut libc::c_void,
}

impl<'inst, 'dev, 'membuf, 'membufslice> std::ops::Deref for MemoryMapping<'inst, 'dev, 'membuf, 'membufslice> {
	type Target = [u8];

	fn deref(&self) -> &[u8] {
		unsafe { std::slice::from_raw_parts(self.buff as _, self.memory_buffer_slice.priv_len as _) }
	}
}

impl<'inst, 'dev, 'membuf, 'membufslice> std::ops::DerefMut for MemoryMapping<'inst, 'dev, 'membuf, 'membufslice> {
	fn deref_mut(&mut self) -> &mut [u8] {
		unsafe { std::slice::from_raw_parts_mut(self.buff as _, self.memory_buffer_slice.priv_len as _) }
	}
}

impl<'inst, 'dev, 'membuf, 'membufslice, T> std::convert::AsRef<T> for MemoryMapping<'inst, 'dev, 'membuf, 'membufslice> {
	fn as_ref(&self) -> &T {
		unsafe { std::mem::transmute(self.buff) }
	}
}

impl<'inst, 'dev, 'membuf, 'membufslice, T> std::convert::AsMut<T> for MemoryMapping<'inst, 'dev, 'membuf, 'membufslice> {
	fn as_mut(&mut self) -> &mut T {
		unsafe { std::mem::transmute(self.buff) }
	}
}

impl<'inst, 'dev, 'membuf, 'membufslice> Drop for MemoryMapping<'inst, 'dev, 'membuf, 'membufslice> {
	fn drop(&mut self) {
		unsafe { self.memory_buffer_slice.membuf.memory.device.procs.UnmapMemory(self.memory_buffer_slice.membuf.memory.device.device, self.memory_buffer_slice.membuf.memory.memory) }
	}
}