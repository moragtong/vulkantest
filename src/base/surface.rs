use std;
use super::*;

const ERROR_NO_DISPLAY_SURFACE_AVAILABLE: u32 = 30;
const ERROR_WINDOW_HAS_BEEN_DESTROYED: u32 = 31;

#[cfg(windows)]
extern "system" {
	fn GetModuleHandleA(_: *const i8) -> *mut std::os::raw::c_void;
}

pub struct Surface<'inst> {
	pub(super) instance:	&'inst super::instance::Instance,
	pub(super) surface:		vk_sys::SurfaceKHR,
	pub(super) width:		u32,
	pub(super) height:		u32,
}

impl<'inst> Surface<'inst> {
	pub(super) fn get_physical_device_memory_properties(&self, physical: vk_sys::PhysicalDevice) -> vk_sys::PhysicalDeviceMemoryProperties {
		unsafe {
			let mut properties = std::mem::uninitialized();
			self.instance.procs.GetPhysicalDeviceMemoryProperties(physical, &mut properties);
			properties
		}
	}

	pub(super) fn get_physical_device_surface_support_khr(&self, physical: vk_sys::PhysicalDevice, i: u32) -> VkResult<vk_sys::Bool32> {
		unsafe {
			let mut ret = std::mem::uninitialized();
			let res = self.instance.procs.GetPhysicalDeviceSurfaceSupportKHR(physical, i, self.surface, &mut ret);
			if res == vk_sys::SUCCESS {
				Ok(ret)
			} else {
				Err(res)
			}
		}
	}

	pub(super) fn get_physical_device_surface_capabilities(&self, physical: vk_sys::PhysicalDevice) -> VkResult<vk_sys::SurfaceCapabilitiesKHR> {
		unsafe {
			let mut capabilities = std::mem::uninitialized();
			let res = self.instance.procs.GetPhysicalDeviceSurfaceCapabilitiesKHR(physical, self.surface, &mut capabilities);
			if res == vk_sys::SUCCESS {
				Ok(capabilities)
			} else {
				Err(res)
			}
		}
	}

	pub(super) fn get_physical_device_surface_formats(&self, physical: vk_sys::PhysicalDevice, formats: &mut Vec<vk_sys::SurfaceFormatKHR>) -> VkResult<usize> {
		unsafe {
			let mut count = std::mem::uninitialized();
			let res = self.instance.procs.GetPhysicalDeviceSurfaceFormatsKHR(physical, self.surface, &mut count, 0 as _);
			if res != vk_sys::SUCCESS {
				return Err(res)
			}
			formats.reserve(count as _);
			if count != 0 {
				let res = self.instance.procs.GetPhysicalDeviceSurfaceFormatsKHR(physical, self.surface, &mut count, formats.as_mut_ptr());
				if res != vk_sys::SUCCESS {
					return Err(res)
				}
				let prevlen = formats.len();
				formats.set_len(prevlen + count as usize);
			}
			Ok(count as _)
		}
	}

	pub(super) fn get_physical_device_surface_present_modes(&self, physical: vk_sys::PhysicalDevice, present_modes: &mut Vec<vk_sys::PresentModeKHR>) -> VkResult<usize> {
		unsafe {
			let mut count = std::mem::uninitialized();
			let res = self.instance.procs.GetPhysicalDeviceSurfacePresentModesKHR(physical, self.surface, &mut count, 0 as _);
			if res != vk_sys::SUCCESS {
				return Err(res)
			}
			present_modes.reserve(count as _);
			if count != 0 {
				let res = self.instance.procs.GetPhysicalDeviceSurfacePresentModesKHR(physical, self.surface, &mut count, present_modes.as_mut_ptr());
				if res != vk_sys::SUCCESS {
					return Err(res)
				}
				let prevlen = present_modes.len();
				present_modes.set_len(prevlen + count as usize);
			}
			Ok(count as _)
		}
	}

	pub fn new(instance: &'inst Instance, window: &winit::Window) -> VkResult<Self> {
		unsafe {
			let mut base_surface = std::mem::uninitialized();

			#[cfg(unix)]
			let res = {
				use super::winit::os::unix::WindowExt;
				if let (Some(display), Some(surface)) = (window.get_wayland_display(), window.get_wayland_surface()) {
					instance.procs.CreateWaylandSurfaceKHR(instance.instance, &vk_sys::WaylandSurfaceCreateInfoKHR {
						sType:	vk_sys::STRUCTURE_TYPE_WAYLAND_SURFACE_CREATE_INFO_KHR, display, surface, ..std::mem::zeroed()
					}, 0 as _, &mut base_surface)
				} else if let (Some(dpy), Some(window)) = (window.get_xlib_display(), window.get_xlib_window()) {
					instance.procs.CreateXlibSurfaceKHR(instance.instance, &vk_sys::XlibSurfaceCreateInfoKHR {
						sType:	vk_sys::STRUCTURE_TYPE_XLIB_SURFACE_CREATE_INFO_KHR, dpy, window, ..std::mem::zeroed()
					}, 0 as _, &mut base_surface)
				} else {
					ERROR_NO_DISPLAY_SURFACE_AVAILABLE
				}
			};

			#[cfg(windows)]
			let res = {
				use winit::os::windows::WindowExt;
				instance.procs.CreateWin32SurfaceKHR(instance.instance, &vk_sys::Win32SurfaceCreateInfoKHR {
					sType:		vk_sys::STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR,
					hinstance:	GetModuleHandleA(0 as _),
					hwnd:		window.get_hwnd() as _,
					..std::mem::zeroed()
				}, 0 as _, &mut base_surface);
			};

			if res == vk_sys::SUCCESS {
				let (width, height) = window.get_inner_size().ok_or(ERROR_WINDOW_HAS_BEEN_DESTROYED)?;
				Ok(Surface { instance, surface: base_surface, width, height })
			} else {
				Err(res)
			}
		}
	}
}

impl<'inst> Drop for Surface<'inst> {
	fn drop(&mut self) {
		unsafe { self.instance.procs.DestroySurfaceKHR(self.instance.instance, self.surface, 0 as _) };
		println!("Surface destructed");
	}
}