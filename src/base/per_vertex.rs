use std;
use super::*;
use self::cgmath::InnerSpace;

#[repr(C)]
pub struct PerVertex {
	pub position:	cgmath::Point3<f32>,
	pub normal:		cgmath::Vector3<f32>,
}

impl PerVertex {
	pub const ATTRIBUTE_DESCRIPTIONS: [vk_sys::VertexInputAttributeDescription; 2] = [
		vk_sys::VertexInputAttributeDescription {
			binding:	0,
			location:	0,
			format:		vk_sys::FORMAT_R32G32B32_SFLOAT,
			offset:		0
		},
		vk_sys::VertexInputAttributeDescription {
			binding:	0,
			location:	1,
			format:		vk_sys::FORMAT_R32G32B32_SFLOAT,
			offset:		std::mem::size_of::<cgmath::Point3<f32>>() as _
		}
	];

	pub fn fill(positions: &[[f32; 3]], indices: &[obj::SimplePolygon], outputs: &mut [PerVertex]) {
		debug_assert!(positions.len() == outputs.len());

		let mut normals = Vec::new();
		normals.resize(positions.len(), Vec::with_capacity(3));

		for tri in indices.iter() {
			let normal = (cgmath::Vector3::from(positions[tri[0].0]) - cgmath::Vector3::from(positions[tri[1].0]))
				.cross(cgmath::Vector3::from(positions[tri[1].0]) - cgmath::Vector3::from(positions[tri[2].0]));

			normals[tri[0].0].push(normal);
			normals[tri[1].0].push(normal);
			normals[tri[2].0].push(normal);
		}

		for ((position, normals), output) in positions.iter().zip(normals.iter()).zip(outputs.iter_mut()) {
			*output = PerVertex {
				position:	(*position).into(),
				normal:		((normals[0] + normals[1] + normals[2]) / 3.).normalize()
			}
		}
	}
}