use std;
use super::*;

pub struct MemoryBuffer<'inst, 'dev>
where 'inst: 'dev {
	pub(super) buffer:		vk_sys::Buffer,
	pub(super) memory:		Memory<'inst, 'dev>,
	pub(super) usage:		u32,
	pub(super) properties:	u32,
}

impl<'inst, 'dev> MemoryBuffer<'inst, 'dev> {
	pub unsafe fn get_unchecked<'this>(&'this self, range: std::ops::Range<vk_sys::DeviceSize>) -> MemoryBufferSlice<'inst, 'dev, 'this> {
		MemoryBufferSlice {
			membuf:		self,
			offset:		range.start,
			priv_len:	range.end - range.start,
		}
	}

	/*pub fn get<'this>(&'this self, range: std::ops::Range<vk_sys::DeviceSize>) -> Option<MemoryBufferSlice<'inst, 'dev, 'this>> {
		if range.start < self.priv_len && range.end <= self.priv_len && range.start < range.end {
			Some(MemoryBufferSlice {
				membuf:	self,
				offset:	range.start,
				priv_len:	range.end - range.start,
			})
		} else {
			None
		}
	}*/

	pub fn len(&self) -> vk_sys::DeviceSize {
		self.memory.len
	}

	pub fn new(device: &'dev Device<'inst>, priv_len: usize, usage: u32, properties: u32) -> VkResult<Self> {
		unsafe {
			let mut buffer = std::mem::uninitialized();
			let res = device.procs.CreateBuffer(device.device, &vk_sys::BufferCreateInfo {
				sType:			vk_sys::STRUCTURE_TYPE_BUFFER_CREATE_INFO,
				size:			priv_len as _,
				sharingMode:	vk_sys::SHARING_MODE_EXCLUSIVE,
				usage, ..std::mem::zeroed()
			}, 0 as _, &mut buffer);

			if res != vk_sys::SUCCESS {
				return Err(res)
			}

			let mut requirements = std::mem::uninitialized();
			device.procs.GetBufferMemoryRequirements(device.device, buffer, &mut requirements);

			let memory = Memory::new(device, &requirements, properties)?;

			let res = device.procs.BindBufferMemory(device.device, buffer, memory.memory, 0);

			if res == vk_sys::SUCCESS {
				Ok(Self { buffer, memory, usage, properties })
			} else {
				Err(res)
			}
		}
	}
}

impl<'inst, 'dev> Drop for MemoryBuffer<'inst, 'dev> {
	fn drop(&mut self) {
		unsafe {
			self.memory.device.procs.DestroyBuffer(self.memory.device.device, self.buffer, 0 as _);
		}

		println!("MemoryBuffer destructed");
	}
}

pub struct MemoryBufferSlice<'inst, 'dev, 'membuf>
where 'inst: 'dev, 'dev: 'membuf {
	pub(super) membuf:		&'membuf MemoryBuffer<'inst, 'dev>,
	pub(super) offset:		vk_sys::DeviceSize,
	pub(super) priv_len:	vk_sys::DeviceSize,
}

impl<'inst, 'dev, 'membuf> MemoryBufferSlice<'inst, 'dev, 'membuf> {
	/*pub fn len(&self) -> vk_sys::DeviceSize {
		self.priv_len
	}*/

	pub fn map_memory<'this>(&'this self) -> VkResult<MemoryMapping<'inst, 'dev, 'membuf, 'this>> {
		debug_assert!(self.membuf.properties & (vk_sys::MEMORY_PROPERTY_HOST_VISIBLE_BIT | vk_sys::MEMORY_PROPERTY_HOST_COHERENT_BIT) == self.membuf.properties);

		unsafe {
			let mut buff = std::mem::uninitialized();

			let res = self.membuf.memory.device.procs.MapMemory(self.membuf.memory.device.device, self.membuf.memory.memory, self.offset, self.priv_len, 0, &mut buff) as _;
			if res == vk_sys::SUCCESS {
				Ok(MemoryMapping { memory_buffer_slice: self, buff: buff as _ })
			} else {
				Err(res)
			}
		}
	}
}