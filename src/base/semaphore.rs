use std;
use super::*;

pub struct Semaphore<'inst, 'dev>
where 'inst: 'dev {
	pub(super) device:		&'dev Device<'inst>,
	pub(super) semaphore:	vk_sys::Semaphore,
}

impl<'inst, 'dev> Semaphore<'inst, 'dev> {
	pub fn new(device: &'dev Device<'inst>) -> VkResult<Self> {
		unsafe {
			let mut semaphore = std::mem::uninitialized();
			let res = device.procs.CreateSemaphore(device.device, &vk_sys::SemaphoreCreateInfo {
				sType:	vk_sys::STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
				..std::mem::zeroed()
			}, 0 as _, &mut semaphore);
			if res == vk_sys::SUCCESS {
				Ok(Self { device, semaphore })
			} else {
				Err(res)
			}
		}
	}
}

impl<'inst, 'dev> Drop for Semaphore<'inst, 'dev> {
	fn drop(&mut self) {
		unsafe { self.device.procs.DestroySemaphore(self.device.device, self.semaphore, 0 as _) };
		println!("Semaphore destructed");
	}
}