use std;
use super::*;

pub struct SwapChainImage<'inst, 'dev, 'swapchain, 'pool, 'cmdbuffer> 
where 'dev: 'pool, 'pool: 'cmdbuffer, 'dev: 'swapchain, 'inst: 'dev {
	pub(super) swapchain:		&'swapchain SwapChain<'inst, 'dev>,
	pub(super) command_buffer:	&'cmdbuffer Fence<'inst, 'dev, CommandBuffer<'inst, 'dev, 'pool>>,
	pub(super) index:			u32,
}

impl<'inst, 'dev, 'swapchain, 'pool, 'cmdbuffer> SwapChainImage<'inst, 'dev, 'swapchain, 'pool, 'cmdbuffer> {
	pub fn submit(&self) -> VkResult<()> {
		let res = unsafe {
			self.swapchain.device.procs.QueueSubmit(self.swapchain.device.graphics_queue, 1, &vk_sys::SubmitInfo {
				sType:					vk_sys::STRUCTURE_TYPE_SUBMIT_INFO,
				pWaitDstStageMask:		&vk_sys::PIPELINE_STAGE_TOP_OF_PIPE_BIT,
				waitSemaphoreCount:		1,
				pWaitSemaphores:		&self.command_buffer.object.semaphore.semaphore,
				commandBufferCount:		1,
				pCommandBuffers:		&self.command_buffer.object.command_buffer,
				signalSemaphoreCount:	1,
				pSignalSemaphores:		&self.swapchain.semaphores.get_unchecked(self.index as usize).semaphore,
				..std::mem::zeroed()
			}, self.command_buffer.signal())
		};

		if res == vk_sys::SUCCESS {
			Ok(())
		} else {
			Err(res)
		}
	}

	pub fn present(self) -> VkResult<()> {
		let res = unsafe {
			self.swapchain.device.procs.QueuePresentKHR(self.swapchain.device.present_queue, &vk_sys::PresentInfoKHR {
				sType:				vk_sys::STRUCTURE_TYPE_PRESENT_INFO_KHR,
				waitSemaphoreCount:	1,
				pWaitSemaphores:	&self.swapchain.semaphores.get_unchecked(self.index as usize).semaphore,
				swapchainCount:		1,
				pSwapchains:		&self.swapchain.swapchain,
				pImageIndices:		&self.index,
				..std::mem::zeroed()
			})
		};

		if res == vk_sys::SUCCESS {
			Ok(())
		} else {
			Err(res)
		}
	}
}