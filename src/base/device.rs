use std;
use super::*;

use util::TryFrom;

const ERROR_NO_PHYSICAL_DEVICES_AVAILABLE: u32 = 22;
const KHR_SWAPCHAIN_EXTENSION_NAME: &[u8] = b"VK_KHR_swapchain\0";

fn choose_swapchain_surface_format(formats: &[vk_sys::SurfaceFormatKHR]) -> vk_sys::SurfaceFormatKHR {
	debug_assert!(!formats.is_empty());

	if let Ok([formats]) = <&[_; 1]>::try_from(formats) {
		if formats.format == vk_sys::FORMAT_UNDEFINED {
			return vk_sys::SurfaceFormatKHR {
				format:		vk_sys::FORMAT_B8G8R8A8_UNORM,
				colorSpace:	vk_sys::COLOR_SPACE_SRGB_NONLINEAR_KHR,
			}
		}
	}
	
	unsafe {
		std::mem::transmute_copy(formats.iter().find(|format|
			format.format == vk_sys::FORMAT_B8G8R8A8_UNORM && format.colorSpace == vk_sys::COLOR_SPACE_SRGB_NONLINEAR_KHR)
		.unwrap_or(formats.get_unchecked(0)))
	}
}

fn choose_swapchain_presentation_mode(presentation_modes: &[vk_sys::PresentModeKHR]) -> vk_sys::PresentModeKHR {
	*presentation_modes.iter().find(|&&mode| mode == vk_sys::PRESENT_MODE_MAILBOX_KHR)
	.or_else(|| presentation_modes.iter().find(|&&mode| mode == vk_sys::PRESENT_MODE_IMMEDIATE_KHR))
	.unwrap_or(&vk_sys::PRESENT_MODE_FIFO_KHR)
}

fn calc_extent(capabilities: &vk_sys::SurfaceCapabilitiesKHR, width: u32, height: u32) -> vk_sys::Extent2D {
	if capabilities.currentExtent.width == std::u32::MAX {
		vk_sys::Extent2D {
			width:	std::cmp::max(capabilities.minImageExtent.width, std::cmp::min(capabilities.maxImageExtent.width, width)),
			height:	std::cmp::max(capabilities.minImageExtent.height, std::cmp::min(capabilities.maxImageExtent.height, height)),
		}
	} else {
		unsafe { std::mem::transmute_copy(&capabilities.currentExtent) }
	}
}

unsafe fn get_queue(device: vk_sys::Device, procs: &vk_sys::DevicePointers, i: u32) -> vk_sys::Queue {
	let mut queue = std::mem::uninitialized();
	procs.GetDeviceQueue(device, i, 0, &mut queue);
	queue
}

pub struct Device<'inst> {
	pub(super) physical:				vk_sys::PhysicalDevice,
	pub(super) device:					vk_sys::Device,
	pub(super) procs:					vk_sys::DevicePointers,
	pub(super) graphics_family_index:	u32,
	pub extent:							vk_sys::Extent2D,
	pub(super) graphics_queue:			vk_sys::Queue,
	pub(super) present_queue:			vk_sys::Queue,
	pub(super) capabilities:			vk_sys::SurfaceCapabilitiesKHR,
	pub(super) format:					vk_sys::SurfaceFormatKHR,
	pub(super) present_mode:			vk_sys::PresentModeKHR,
	pub(super) surface:					&'inst Surface<'inst>,
}

impl<'inst> Device<'inst> {
	pub fn update_descriptor_set<'dev>(&'dev self, descriptor_set: &DescriptorSet<'inst, 'dev, '_>, slice: &MemoryBufferSlice<'inst, 'dev, '_>) {
		unsafe {
			self.procs.UpdateDescriptorSets(self.device, 1, &vk_sys::WriteDescriptorSet {
				sType:				vk_sys::STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
				descriptorType:		vk_sys::DESCRIPTOR_TYPE_UNIFORM_BUFFER,
				dstSet:				descriptor_set.0,
				descriptorCount:	1,
				pBufferInfo:		&vk_sys::DescriptorBufferInfo {
					buffer:	slice.membuf.buffer,
					offset:	slice.offset,
					range:	slice.priv_len,
				}, ..std::mem::zeroed()
			}, 0, 0 as _)
		}
	}

	pub fn new(surface: &'inst Surface<'inst>, physicals: &[vk_sys::PhysicalDevice]) -> VkResult<Self> {
		let mut formats = Default::default();
		let mut present_modes = Default::default();
		let mut properties = Default::default();
		let mut family_properties = Default::default();

		for &physical in physicals.iter() {
			//let features = vulkan_rs::cmds::vkGetPhysicalDeviceFeatures(physical);
			if surface.get_physical_device_surface_formats(physical, &mut formats)? != 0
			&& surface.get_physical_device_surface_present_modes(physical, &mut present_modes)? != 0
			&& surface.instance.get_device_extension_properties(physical, &mut properties)? != 0 {
				if properties.iter().find(|property|
					unsafe { libc::strncmp(property.extensionName.as_ptr(), KHR_SWAPCHAIN_EXTENSION_NAME.as_ptr() as _, property.extensionName.len()) } == 0
				).is_some() {
					surface.instance.get_physical_device_queue_family_properties(physical, &mut family_properties);

					let mut graphics_index = None;
					let mut presentation_index = None;

					for (i, family) in family_properties.iter().enumerate() {
						if family.queueFlags & vk_sys::QUEUE_GRAPHICS_BIT != 0 {
							graphics_index = Some(i as u32);
							if surface.get_physical_device_surface_support_khr(physical, i as _)? != 0 {
								presentation_index = Some(i as u32);
								break
							}
						}

						if surface.get_physical_device_surface_support_khr(physical, i as _)? != 0 {
							presentation_index = Some(i as u32);
						}
					}

					if let (Some(graphics_family_index), Some(present_i)) = (graphics_index, presentation_index) {
						let mut device = unsafe { std::mem::uninitialized() };
						let infos = [
							vk_sys::DeviceQueueCreateInfo {
								sType:				vk_sys::STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
								queueFamilyIndex:	graphics_family_index as _,
								queueCount:			1,
								pQueuePriorities:	&1.0,
								..unsafe { std::mem::zeroed() }
							},
							vk_sys::DeviceQueueCreateInfo {
								sType:				vk_sys::STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
								queueFamilyIndex:	present_i as _,
								queueCount:			1,
								pQueuePriorities:	&1.0,
								..unsafe { std::mem::zeroed() }
							}
						];
						let res = unsafe {
							surface.instance.procs.CreateDevice(physical, &vk_sys::DeviceCreateInfo {
								sType:						vk_sys::STRUCTURE_TYPE_DEVICE_CREATE_INFO,
								pEnabledFeatures:			&vk_sys::PhysicalDeviceFeatures {
									geometryShader:	vk_sys::TRUE,
									..std::mem::zeroed()
								},
								queueCreateInfoCount:		if graphics_family_index == present_i { 1 } else { 2 },
								pQueueCreateInfos:			infos.as_ptr(),
								enabledExtensionCount:		1,
								ppEnabledExtensionNames:	&(KHR_SWAPCHAIN_EXTENSION_NAME.as_ptr() as _),
								..std::mem::zeroed()
							}, 0 as _, &mut device)
						};
						if res != vk_sys::SUCCESS {
							return Err(res)
						}
						let mut procs = vk_sys::DevicePointers::load(|function| unsafe { surface.instance.procs.GetDeviceProcAddr(device, function.as_ptr()) } as _);
						let capabilities = surface.get_physical_device_surface_capabilities(physical)?;

						return Ok(Self {
							graphics_queue:	unsafe { get_queue(device, &procs, graphics_family_index as _) },
							present_queue:	unsafe { get_queue(device, &procs, present_i as _) },
							extent:			calc_extent(&capabilities, surface.width as _, surface.height as _),
							format:			choose_swapchain_surface_format(&formats),
							present_mode:	choose_swapchain_presentation_mode(&present_modes),
							physical, device, procs, graphics_family_index, capabilities, surface
						})
					}
					family_properties.clear();
					properties.clear();
					present_modes.clear();
					formats.clear();
				}
			}	
		}
		Err(ERROR_NO_PHYSICAL_DEVICES_AVAILABLE)
	}
}

impl<'inst> Drop for Device<'inst> {
	fn drop(&mut self) {
		unsafe {	
			self.procs.DestroyDevice(self.device, 0 as _);
		}
		println!("Device destructed");
	}
}