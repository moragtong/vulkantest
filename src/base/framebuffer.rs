use std;
use super::*;

pub struct Framebuffer<'inst, 'dev, 'color, 'depth>
where 'inst: 'dev, 'dev: 'color, 'dev: 'depth {
	pub(super) device:		&'dev Device<'inst>,
	_color:					ImageView<'inst, 'dev, 'color>,
	_depth:					DepthImageView<'inst, 'dev, 'depth>,
	pub(super) framebuffer:	vk_sys::Framebuffer,
}

impl<'inst, 'dev, 'color, 'depth> Framebuffer<'inst, 'dev, 'color, 'depth> {
	pub fn new(render_pass: &RenderPass<'inst, 'dev>, _color: ImageView<'inst, 'dev, 'color>, _depth: DepthImageView<'inst, 'dev, 'depth>) -> VkResult<Self> {
		let attachments = [_color.view, _depth.view];
		unsafe {
			let mut framebuffer = std::mem::uninitialized();
			let res = render_pass.device.procs.CreateFramebuffer(render_pass.device.device, &vk_sys::FramebufferCreateInfo {
				sType:				vk_sys::STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
				renderPass:			render_pass.render_pass,
				attachmentCount:	attachments.len() as _,
				pAttachments:		attachments.as_ptr(),
				width:				render_pass.device.extent.width,
				height:				render_pass.device.extent.height,
				layers:				1,
				..std::mem::zeroed()
			}, 0 as _, &mut framebuffer);

			if res == vk_sys::SUCCESS {
				Ok(Framebuffer { device: render_pass.device, _color, _depth, framebuffer })
			} else {
				Err(res)
			}
		}
	}
}

impl<'inst, 'dev, 'color, 'depth> Drop for Framebuffer<'inst, 'dev, 'color, 'depth> {
	fn drop(&mut self) {
		unsafe { self.device.procs.DestroyFramebuffer(self.device.device, self.framebuffer, 0 as _) };
		println!("Framebuffer destructed");
	}
}