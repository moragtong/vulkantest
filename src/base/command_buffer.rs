use std;
use super::*;

pub struct CommandBuffer<'inst, 'dev, 'pool>
where 'dev: 'pool, 'inst: 'dev {
	pub(super) semaphore:		Semaphore<'inst, 'dev>,
	pub(super) command_buffer:	vk_sys::CommandBuffer,
	pub(super) marker:			std::marker::PhantomData<&'pool ()>
}

impl<'inst, 'dev, 'pool> CommandBuffer<'inst, 'dev, 'pool> {
	pub fn record<'this>(&'this self) -> VkResult<CommandBufferRecord<'inst, 'dev, 'pool, 'this>> {
		let res = unsafe {
			self.semaphore.device.procs.BeginCommandBuffer(self.command_buffer, &vk_sys::CommandBufferBeginInfo {
				sType:	vk_sys::STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
				flags:	vk_sys::COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT,
				..std::mem::zeroed()
			})
		};
		if res == vk_sys::SUCCESS {
			Ok(CommandBufferRecord(self))
		} else {
			Err(res)
		}
	}

	/*pub fn reset(&self) -> VkResult<()> {
		let res = unsafe { self.semaphore.device.procs.ResetCommandBuffer(self.command_buffer, 0) };
		if res == vk_sys::SUCCESS {
			Ok(())
		} else {
			Err(res)
		}
	}*/
}

impl<'inst, 'dev, 'pool> Fence<'inst, 'dev, CommandBuffer<'inst, 'dev, 'pool>> {
	pub fn submit(&self) -> VkResult<()> {
		let object = self.acquire()?;
		let res = unsafe {
			self.device.procs.QueueSubmit(self.device.graphics_queue, 1, &vk_sys::SubmitInfo {
				sType:					vk_sys::STRUCTURE_TYPE_SUBMIT_INFO,
				commandBufferCount:		1,
				pCommandBuffers:		&object.command_buffer,
				..std::mem::zeroed()
			}, self.signal())
		};

		if res == vk_sys::SUCCESS {
			Ok(())
		} else {
			Err(res)
		}
	}
}

pub struct CommandBufferRecord<'inst, 'dev, 'pool, 'cmdbuffer>(&'cmdbuffer CommandBuffer<'inst, 'dev, 'pool>)
where 'dev: 'pool, 'inst: 'dev, 'pool: 'cmdbuffer;

impl<'inst, 'dev, 'pool, 'cmdbuffer> CommandBufferRecord<'inst, 'dev, 'pool, 'cmdbuffer> {
	pub fn render_pass<'this, T: Copy + Clone>(&'this self, render_pass: &RenderPass<'inst, 'dev>, framebuffer: &Framebuffer<'inst, 'dev, '_, '_>)
	-> CommandBufferRenderPass<'inst, 'dev, 'pool, 'cmdbuffer, 'this, T> {
		let clear_values = [
			vk_sys::ClearValue {
				color:	vk_sys::ClearColorValue {
					float32:	[0., 0., 0., 1.]
				}
			},
			vk_sys::ClearValue {
				depthStencil:	vk_sys::ClearDepthStencilValue {
					depth:		1.0,
					stencil:	0,
				}
			},
		];

		unsafe {
			self.0.semaphore.device.procs.CmdBeginRenderPass(self.0.command_buffer, &vk_sys::RenderPassBeginInfo {
				sType:				vk_sys::STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
				renderPass:			render_pass.render_pass,
				renderArea:			vk_sys::Rect2D { extent: std::mem::transmute_copy(&render_pass.device.extent), ..std::mem::zeroed() },
				clearValueCount:	clear_values.len() as _,
				pClearValues:		clear_values.as_ptr(),
				framebuffer:		framebuffer.framebuffer,
				..std::mem::zeroed()
			}, vk_sys::SUBPASS_CONTENTS_INLINE)
		}

		CommandBufferRenderPass(self, Default::default())
	}

	pub fn copy<'membuf>(&self, dst: &MemoryBufferSlice<'inst, 'dev, 'membuf>, src: &MemoryBufferSlice<'inst, 'dev, 'membuf>)
	where 'dev: 'membuf {
		debug_assert!(dst.priv_len == src.priv_len);
		unsafe {
			self.0.semaphore.device.procs.CmdCopyBuffer(self.0.command_buffer, src.membuf.buffer, dst.membuf.buffer, 1, &vk_sys::BufferCopy {
				srcOffset:	src.offset,
				dstOffset:	dst.offset,
				size:		dst.priv_len,
			})
		}
	}

	pub fn end(self) -> VkResult<()> {
		let res = unsafe { self.0.semaphore.device.procs.EndCommandBuffer(self.0.command_buffer) };
		std::mem::forget(self);
		if res == vk_sys::SUCCESS {
			Ok(())
		} else {
			Err(res)
		}
	}
}

impl<'inst, 'dev, 'pool, 'cmdbuffer> Drop for CommandBufferRecord<'inst, 'dev, 'pool, 'cmdbuffer> {
	fn drop(&mut self) {
		unsafe { self.0.semaphore.device.procs.EndCommandBuffer(self.0.command_buffer) };
	}
}

pub struct CommandBufferRenderPass<'inst, 'dev, 'pool, 'cmdbuffer, 'record, T>(&'record CommandBufferRecord<'inst, 'dev, 'pool, 'cmdbuffer>, std::marker::PhantomData<T>)
where 'dev: 'pool, 'inst: 'dev, 'pool: 'cmdbuffer, 'cmdbuffer: 'record, T: Clone + Copy;

impl<'inst, 'dev, 'pool, 'cmdbuffer, 'record, T: Clone + Copy> CommandBufferRenderPass<'inst, 'dev, 'pool, 'cmdbuffer, 'record, T> {
	pub fn bind_pipeline<'layout, 'desc_layout: 'layout>(&self, pipeline: &GraphicsPipeline<'inst, 'dev, 'desc_layout, 'layout, T>) {
		unsafe { (self.0).0.semaphore.device.procs.CmdBindPipeline((self.0).0.command_buffer, vk_sys::PIPELINE_BIND_POINT_GRAPHICS, pipeline.pipeline) };
	}

	pub fn push_constants(&self, layout: &PipelineLayout<'inst, 'dev, '_, T>, value: &T) {
		unsafe {
			(self.0).0.semaphore.device.procs.CmdPushConstants((self.0).0.command_buffer, layout.layout,
				vk_sys::SHADER_STAGE_VERTEX_BIT | vk_sys::SHADER_STAGE_GEOMETRY_BIT, 0, std::mem::size_of::<T>() as _, value as *const _ as _)
		}
	}

	pub fn bind_descriptor_sets(&self, layout: &PipelineLayout<'inst, 'dev, '_, T>, descriptor_sets: &[DescriptorSet]) {
		unsafe {
			(self.0).0.semaphore.device.procs.CmdBindDescriptorSets((self.0).0.command_buffer, vk_sys::PIPELINE_BIND_POINT_GRAPHICS, layout.layout, 0, 
				descriptor_sets.len() as _, descriptor_sets.as_ptr() as _, 0, 0 as _)
		}
	}

	pub fn bind_vertex_buffers(&self, vertex_buffer: &MemoryBufferSlice<'inst, 'dev, '_>) {
		debug_assert!(vertex_buffer.membuf.usage | vk_sys::BUFFER_USAGE_VERTEX_BUFFER_BIT == vertex_buffer.membuf.usage);
		unsafe {
			(self.0).0.semaphore.device.procs.CmdBindVertexBuffers((self.0).0.command_buffer, 0, 1, &vertex_buffer.membuf.buffer, &vertex_buffer.offset);
		}
	}

	pub fn bind_index_buffer(&self, index_buffer: &MemoryBufferSlice<'inst, 'dev, '_>) {
		debug_assert!(index_buffer.membuf.usage | vk_sys::BUFFER_USAGE_INDEX_BUFFER_BIT == index_buffer.membuf.usage);
		unsafe {
			(self.0).0.semaphore.device.procs.CmdBindIndexBuffer((self.0).0.command_buffer, index_buffer.membuf.buffer, index_buffer.offset, vk_sys::INDEX_TYPE_UINT32);
		}
	}

	/*pub fn draw(&self, num: u32) {
		unsafe {
			(self.0).0.semaphore.device.procs.CmdDraw((self.0).0.command_buffer, num, 1, 0, 0);
		}
	}*/

	pub fn draw_indexed(&self, num: u32) {
		unsafe {
			(self.0).0.semaphore.device.procs.CmdDrawIndexed((self.0).0.command_buffer, num, 1, 0, 0, 0);
		}
	}
}

impl<'inst, 'dev, 'pool, 'cmdbuffer, 'record, T: Copy + Clone> Drop for CommandBufferRenderPass<'inst, 'dev, 'pool, 'cmdbuffer, 'record, T> {
	fn drop(&mut self) {
		unsafe { (self.0).0.semaphore.device.procs.CmdEndRenderPass((self.0).0.command_buffer) }
	}
}