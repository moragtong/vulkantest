use std;
use super::*;

pub struct ShaderModule<'inst, 'dev>
where 'inst: 'dev {
	pub(super) device:	&'dev Device<'inst>,
	pub(super) module:	vk_sys::ShaderModule,
}

impl<'inst, 'dev> ShaderModule<'inst, 'dev> {
	pub fn new(device: &'dev Device<'inst>, code: &[u8]) -> VkResult<Self> {
		unsafe {
			let mut module = std::mem::uninitialized();
			let res = device.procs.CreateShaderModule(device.device, &vk_sys::ShaderModuleCreateInfo {
				sType:		vk_sys::STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
				codeSize:	code.len() as _,
				pCode:		code.as_ptr() as _,
				..std::mem::zeroed()
			}, 0 as _, &mut module);

			if res != vk_sys::SUCCESS {
				return Err(res)
			}

			Ok(Self { device, module })
		}
	}
}

impl<'inst, 'dev> Drop for ShaderModule<'inst, 'dev> {
	fn drop(&mut self) {
		unsafe {
			self.device.procs.DestroyShaderModule(self.device.device, self.module, 0 as _);
		}
		println!("ShaderModules destructed");
	}
}