use std;
use super::*;

use std::iter::FromIterator;

pub struct SwapChain<'inst, 'dev>
where 'inst: 'dev {
	pub(super) device:		&'dev Device<'inst>,
	pub(super) swapchain:	vk_sys::SwapchainKHR,
	pub(super) _images:		Box<[vk_sys::Image]>,
	pub(super) semaphores:	Box<[Semaphore<'inst, 'dev>]>,
}

impl<'inst, 'dev> SwapChain<'inst, 'dev> {
	pub fn acquire_next_image<'swapchain, 'pool, 'cmdbuffer, 'orig, 'framebuffer>(&'swapchain self,
		command_buffer:	&'cmdbuffer Fence<'inst, 'dev, CommandBuffer<'inst, 'dev, 'pool>>,
	) -> VkResult<SwapChainImage<'inst, 'dev, 'swapchain, 'pool, 'cmdbuffer>> {
		let object = command_buffer.acquire()?;
		unsafe {
			let mut index = std::mem::uninitialized();
			let res = self.device.procs.AcquireNextImageKHR(self.device.device, self.swapchain, std::u64::MAX, object.semaphore.semaphore, 0, &mut index);
			if res == vk_sys::SUCCESS {
				Ok(SwapChainImage { index, command_buffer, swapchain: self })
			} else {
				Err(res)
			}
		}
	}

	pub fn images(&self) -> &[vk_sys::Image] {
		&self._images
	}

	pub fn new(device: &'dev Device<'inst>, surface: &'inst Surface<'inst>) -> VkResult<Self> {
		unsafe {
			let mut swapchain = std::mem::uninitialized();
			let res = device.procs.CreateSwapchainKHR(device.device, &vk_sys::SwapchainCreateInfoKHR {
				sType:				vk_sys::STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
				surface:			surface.surface,
				minImageCount:
					if device.capabilities.maxImageCount > 0 && device.capabilities.minImageCount == device.capabilities.maxImageCount {
						device.capabilities.maxImageCount
					} else {
						device.capabilities.minImageCount + 1
					},
				imageFormat:		device.format.format,
				imageColorSpace:	device.format.colorSpace,
				imageExtent:		std::mem::transmute_copy(&device.extent),
				imageUsage:			vk_sys::IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
				imageArrayLayers:	1,
				imageSharingMode:	vk_sys::SHARING_MODE_EXCLUSIVE,
				preTransform:		device.capabilities.currentTransform,
				compositeAlpha:		vk_sys::COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
				presentMode:		device.present_mode,
				clipped:			vk_sys::TRUE,
				..std::mem::zeroed()
			}, 0 as _, &mut swapchain);

			if res != vk_sys::SUCCESS {
				return Err(res);
			}

			let mut count = std::mem::uninitialized();
			let res = device.procs.GetSwapchainImagesKHR(device.device, swapchain, &mut count, 0 as _);
			if res != vk_sys::SUCCESS {
				return Err(res);
			}

			let mut images = Vec::with_capacity(count as usize);
			let res = device.procs.GetSwapchainImagesKHR(device.device, swapchain, &mut count, images.as_mut_ptr());
			if res != vk_sys::SUCCESS {
				return Err(res);
			}

			images.set_len(count as usize);

			Ok(SwapChain {
				semaphores:	VkResult::<Vec<_>>::from_iter((0..images.len())
					.map(|_| Semaphore::new(&device)))?.into(),
				_images:		images.into(),
				swapchain, device,
			})
		}
	}
}

impl<'inst, 'dev> Drop for SwapChain<'inst, 'dev> {
	fn drop(&mut self) {
		unsafe { self.device.procs.DestroySwapchainKHR(self.device.device, self.swapchain, 0 as _) };
		println!("Swapchain destructed");
	}
}