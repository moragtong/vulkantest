use std;
use super::*;

pub struct ImageView<'inst, 'dev, 'orig>
where 'dev: 'orig, 'inst: 'dev {
	pub(super) device:	&'dev Device<'inst>,
	pub(super) view:	vk_sys::ImageView,
	marker:				std::marker::PhantomData<&'orig vk_sys::Image>
}

impl<'inst, 'dev, 'orig> ImageView<'inst, 'dev, 'orig> {
	pub fn new(device: &'dev Device<'inst>, image: &'orig vk_sys::Image) -> VkResult<Self> {
		unsafe {
			let mut view = std::mem::uninitialized();
			let res = device.procs.CreateImageView(device.device, &vk_sys::ImageViewCreateInfo {
				sType:				vk_sys::STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
				image:				*image,
				viewType:			vk_sys::IMAGE_VIEW_TYPE_2D,
				format:				device.format.format,
				components:			vk_sys::ComponentMapping {
					r:	vk_sys::COMPONENT_SWIZZLE_IDENTITY,
					g:	vk_sys::COMPONENT_SWIZZLE_IDENTITY,
					b:	vk_sys::COMPONENT_SWIZZLE_IDENTITY,
					a:	vk_sys::COMPONENT_SWIZZLE_IDENTITY,
				},
				subresourceRange:	vk_sys::ImageSubresourceRange {
					aspectMask:		vk_sys::IMAGE_ASPECT_COLOR_BIT,
					levelCount:		1,
					layerCount:		1,
					..std::mem::zeroed()
				},
				..std::mem::zeroed()
			}, 0 as _, &mut view);

			if res == vk_sys::SUCCESS {
				Ok(Self { device, view, marker: Default::default() })
			} else {
				Err(res)
			}
		}
	}
}

impl<'inst, 'dev, 'orig> Drop for ImageView<'inst, 'dev, 'orig> {
	fn drop(&mut self) {
		unsafe { self.device.procs.DestroyImageView(self.device.device, self.view, 0 as _) };
		println!("ImageView destructed");
	}
}