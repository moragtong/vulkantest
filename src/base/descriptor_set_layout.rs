use std;
use super::*;

pub struct DescriptorSetLayout<'inst, 'dev>
where 'inst: 'dev {
	pub(super) device:	&'dev Device<'inst>,
	pub(super) layout:	vk_sys::DescriptorSetLayout
}

impl<'inst, 'dev> DescriptorSetLayout<'inst, 'dev> {
	pub fn new(device: &'dev Device<'inst>) -> VkResult<Self> {
		unsafe {
			let mut layout = std::mem::uninitialized();

			let res = device.procs.CreateDescriptorSetLayout(device.device, &vk_sys::DescriptorSetLayoutCreateInfo {
				sType:			vk_sys::STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
				bindingCount:	1,
				pBindings:		&vk_sys::DescriptorSetLayoutBinding {
					binding:			0,
					descriptorType:		vk_sys::DESCRIPTOR_TYPE_UNIFORM_BUFFER,
					descriptorCount:	1,
					stageFlags:			vk_sys::SHADER_STAGE_VERTEX_BIT | vk_sys::SHADER_STAGE_GEOMETRY_BIT,
					..std::mem::zeroed()
				}, ..std::mem::zeroed()
			}, 0 as _, &mut layout);

			if res == vk_sys::SUCCESS {
				Ok(Self { device, layout })
			} else {
				Err(res)
			}
		}
	}
}

impl<'inst, 'dev> Drop for DescriptorSetLayout<'inst, 'dev> {
	fn drop(&mut self) {
		unsafe {
			self.device.procs.DestroyDescriptorSetLayout(self.device.device, self.layout, 0 as _);
		}
	}
}