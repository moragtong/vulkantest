use std;
use super::*;

pub struct DepthImage<'inst, 'dev>
where 'inst: 'dev {
	pub(super) memory:	Memory<'inst, 'dev>,
	pub(super) image:	vk_sys::Image,
}

impl<'inst, 'dev> DepthImage<'inst, 'dev> {
	pub fn new(device: &'dev Device<'inst>) -> VkResult<Self> {
		unsafe {
			let mut image = std::mem::uninitialized();
			let res = device.procs.CreateImage(device.device, &vk_sys::ImageCreateInfo {
				sType:			vk_sys::STRUCTURE_TYPE_IMAGE_CREATE_INFO,
				imageType:		vk_sys::IMAGE_TYPE_2D,
				format:			vk_sys::FORMAT_D32_SFLOAT,
				mipLevels:		1,
				arrayLayers:	1,
				samples:		vk_sys::SAMPLE_COUNT_1_BIT,
				initialLayout:	vk_sys::IMAGE_LAYOUT_UNDEFINED,
				usage:			vk_sys::IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
				sharingMode:	vk_sys::SHARING_MODE_EXCLUSIVE,
				extent:			vk_sys::Extent3D {
					width:	device.extent.width,
					height:	device.extent.height,
					depth:	1,
				}, ..std::mem::zeroed()
			}, 0 as _, &mut image);

			if res != vk_sys::SUCCESS {
				return Err(res)
			}

			let mut requirements = std::mem::uninitialized();
			device.procs.GetImageMemoryRequirements(device.device, image, &mut requirements);

			let memory = Memory::new(device, &requirements, vk_sys::MEMORY_PROPERTY_DEVICE_LOCAL_BIT)?;

			let res = device.procs.BindImageMemory(device.device, image, memory.memory, 0);

			if res == vk_sys::SUCCESS {
				Ok(Self { image, memory })
			} else {
				Err(res)
			}
		}
	}
}

impl<'inst, 'dev> Drop for DepthImage<'inst, 'dev> {
	fn drop(&mut self) {
		unsafe {
			self.memory.device.procs.DestroyImage(self.memory.device.device, self.image, 0 as _);
		}
	}
}