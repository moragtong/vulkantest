use std;
use super::*;

pub struct DepthImageView<'inst, 'dev, 'orig>
where 'dev: 'orig, 'inst: 'dev {
	pub(super) device:	&'dev Device<'inst>,
	pub(super) view:	vk_sys::ImageView,
	marker:				std::marker::PhantomData<&'orig vk_sys::Image>
}

impl<'inst, 'dev, 'orig> DepthImageView<'inst, 'dev, 'orig> {
	pub fn new(image: &'orig DepthImage<'inst, 'dev>) -> VkResult<Self> {
		unsafe {
			let mut view = std::mem::uninitialized();
			let res = image.memory.device.procs.CreateImageView(image.memory.device.device, &vk_sys::ImageViewCreateInfo {
				sType:				vk_sys::STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
				image:				image.image,
				viewType:			vk_sys::IMAGE_VIEW_TYPE_2D,
				format:				vk_sys::FORMAT_D32_SFLOAT,
				components:			vk_sys::ComponentMapping {
					r:	vk_sys::COMPONENT_SWIZZLE_IDENTITY,
					g:	vk_sys::COMPONENT_SWIZZLE_IDENTITY,
					b:	vk_sys::COMPONENT_SWIZZLE_IDENTITY,
					a:	vk_sys::COMPONENT_SWIZZLE_IDENTITY,
				},
				subresourceRange:	vk_sys::ImageSubresourceRange {
					aspectMask:		vk_sys::IMAGE_ASPECT_DEPTH_BIT,
					levelCount:		1,
					layerCount:		1,
					..std::mem::zeroed()
				},
				..std::mem::zeroed()
			}, 0 as _, &mut view);

			if res == vk_sys::SUCCESS {
				Ok(Self { view, device: &image.memory.device, marker: Default::default() })
			} else {
				Err(res)
			}
		}
	}
}

impl<'inst, 'dev, 'orig> Drop for DepthImageView<'inst, 'dev, 'orig> {
	fn drop(&mut self) {
		unsafe { self.device.procs.DestroyImageView(self.device.device, self.view, 0 as _) };
		println!("ImageView destructed");
	}
}