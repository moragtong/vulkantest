use std;
use super::*;

pub struct RenderPass<'inst, 'dev>
where 'inst: 'dev {
	pub(super) device:		&'dev super::device::Device<'inst>,
	pub(super) render_pass:	vk_sys::RenderPass,
}

impl<'inst, 'dev> RenderPass<'inst, 'dev> {
	pub fn new(device: &'dev super::device::Device<'inst>) -> VkResult<Self> {
		unsafe {
			let attachments = [
				vk_sys::AttachmentDescription {
					format:			device.format.format,
					samples:		vk_sys::SAMPLE_COUNT_1_BIT,
					loadOp:			vk_sys::ATTACHMENT_LOAD_OP_CLEAR,
					storeOp:		vk_sys::ATTACHMENT_STORE_OP_STORE,
					stencilLoadOp:	vk_sys::ATTACHMENT_LOAD_OP_DONT_CARE,
					stencilStoreOp:	vk_sys::ATTACHMENT_STORE_OP_DONT_CARE,
					initialLayout:	vk_sys::IMAGE_LAYOUT_UNDEFINED,
					finalLayout:	vk_sys::IMAGE_LAYOUT_PRESENT_SRC_KHR,
					..std::mem::zeroed()
				},
				vk_sys::AttachmentDescription {
					format:			vk_sys::FORMAT_D32_SFLOAT,
					samples:		vk_sys::SAMPLE_COUNT_1_BIT,
					loadOp:			vk_sys::ATTACHMENT_LOAD_OP_CLEAR,
					storeOp:		vk_sys::ATTACHMENT_STORE_OP_DONT_CARE,
					stencilLoadOp:	vk_sys::ATTACHMENT_LOAD_OP_DONT_CARE,
					stencilStoreOp:	vk_sys::ATTACHMENT_STORE_OP_DONT_CARE,
					initialLayout:	vk_sys::IMAGE_LAYOUT_UNDEFINED,
					finalLayout:	vk_sys::IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
					..std::mem::zeroed()
				}
			];
			
			let mut render_pass = std::mem::uninitialized();
			let res = device.procs.CreateRenderPass(device.device, &vk_sys::RenderPassCreateInfo {
				sType:				vk_sys::STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
				attachmentCount:	attachments.len() as _,
				pAttachments:		attachments.as_ptr(),
				subpassCount:		1,
				pSubpasses:			&vk_sys::SubpassDescription {
					pipelineBindPoint:			vk_sys::PIPELINE_BIND_POINT_GRAPHICS,
					colorAttachmentCount:		1,
					pColorAttachments:			&vk_sys::AttachmentReference {
						attachment:	0,
						layout:		vk_sys::IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
					},
					pDepthStencilAttachment:	&vk_sys::AttachmentReference {
						attachment:	1,
						layout:		vk_sys::IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
					},
					..std::mem::zeroed()
				},
				..std::mem::zeroed()
			}, 0 as _, &mut render_pass); 

			if res == vk_sys::SUCCESS {
				Ok(Self { device, render_pass })
			} else {
				Err(res)
			}
		}
	}
}

impl<'inst, 'dev> Drop for RenderPass<'inst, 'dev> {
	fn drop(&mut self) {
		unsafe { self.device.procs.DestroyRenderPass(self.device.device, self.render_pass, 0 as _) };
	}
}