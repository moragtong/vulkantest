#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(triangles_adjacency) in;
layout(triangle_strip, max_vertices = 15) out;

layout(push_constant) uniform PushConstants {
	mat4 model, proj_view;
} push_constants;

layout(binding = 0) uniform Uniform {
	vec3 light;
} global;

layout(location = 0) out float color;

float direction(uint x, uint y, uint z) {
	vec3 u = (gl_in[y].gl_Position - gl_in[x].gl_Position).xyz;
	vec3 v = (gl_in[z].gl_Position - gl_in[y].gl_Position).xyz;

	return dot(global.light - (gl_in[x].gl_Position + gl_in[y].gl_Position + gl_in[z].gl_Position).xyz / 3, cross(u, v));
}

void draw(uint x, uint y, uint z) {
	if (direction(x, y, z) >= 0.0) {
		gl_Position = push_constants.proj_view * gl_in[z].gl_Position;
		EmitVertex();

		gl_Position = push_constants.proj_view * gl_in[x].gl_Position;
		EmitVertex();

		gl_Position = push_constants.proj_view * vec4(gl_in[z].gl_Position.xyz - global.light, 0.0);
		EmitVertex();

		gl_Position = push_constants.proj_view * vec4(gl_in[x].gl_Position.xyz - global.light, 0.0);
		EmitVertex();

		EndPrimitive();
	}
}

void main() {
	color = length(gl_in[0].gl_Position + gl_in[2].gl_Position + gl_in[4].gl_Position);
	
	if (direction(0, 2, 4) < 0.0) {
		gl_Position = push_constants.proj_view * gl_in[0].gl_Position;
		EmitVertex();

		gl_Position = push_constants.proj_view * gl_in[2].gl_Position;
		EmitVertex();

		gl_Position = push_constants.proj_view * gl_in[4].gl_Position;
		EmitVertex();

		EndPrimitive();

		draw(0, 1, 2);
		draw(2, 3, 4);
		draw(4, 5, 0);
	}
}